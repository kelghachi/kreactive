KREACTIVE_API
=============

A Symfony project created on March 29, 2018, 10:40 am.

This project serves an REST API with the name KREACTIVE.

It manages -> users , movies and votes for movies per week

at the end of the week the winner movie by users votes will be revealed 

API documentation : http://kreactive.local/app_dev.php/doc

links to use in order to consume the API : 
  
    http://myapi.com/user (POST, Creates user)
    http://myapi.com/users/chosen (GET, list of users who chosen a movie)
    http://myapi.com/movieChoice/{userId} (GET, list of movies chosen by user)
    http://myapi.com/movieChoice/{id} (DELETE, deletes movieChoice by id)
    http://myapi.com/movieChoice (POST, store users movie choice)
    http://myapi.com/bestMovie (POST, reveals the movie winner)
    
    

