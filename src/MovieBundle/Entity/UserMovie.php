<?php

namespace MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserMovie
 *
 * @ORM\Table(name="user_movie")
 * @ORM\Entity(repositoryClass="MovieBundle\Repository\UserMovieRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class UserMovie
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="listChoices")
     * @ORM\JoinColumn(name="user_id",referencedColumnName="id")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="Movie", inversedBy="listChoices")
     * @ORM\JoinColumn(name="movie_id",referencedColumnName="id")
     * @Serializer\Expose()
     */
    private $movie;

    /**
     * @var \DateTime
     * @ORM\Column(name="chosed_at", type="datetime")
     * @Serializer\Expose()
     */
    private $chosedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chosedAt
     *
     * @param \DateTime $chosedAt
     *
     * @return UserMovie
     */
    public function setChosedAt($chosedAt)
    {
        $this->chosedAt = $chosedAt;

        return $this;
    }

    /**
     * Get chosedAt
     *
     * @return \DateTime
     */
    public function getChosedAt()
    {
        return $this->chosedAt;
    }

    /**
     * Set user
     *
     * @param \MovieBundle\Entity\User $user
     *
     * @return UserMovie
     */
    public function setUser(\MovieBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \MovieBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set movie
     *
     * @param \MovieBundle\Entity\Movie $movie
     *
     * @return UserMovie
     */
    public function setMovie(\MovieBundle\Entity\Movie $movie = null)
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * Get movie
     *
     * @return \MovieBundle\Entity\Movie
     */
    public function getMovie()
    {
        return $this->movie;
    }
}
