<?php

namespace MovieBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\FOSRestController;
use MovieBundle\Entity\User;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Serializer;

class UserController extends FOSRestController
{

    /**
     * @ApiDoc(
     * resource="/users/chosen",
     * description="Sends the list of users who choose a movie",
     * statusCodes={
     *      200 = "List of users",
     *      404 = "No user Found"
     *     }
     * )
     *
     * @Rest\Get("/users/chosen")
     * @return View
     */
    public function getUsersChosenAction(){
        $qb = $this->getDoctrine()->getManager()->createQueryBuilder();
        $qb->select('u')->from('MovieBundle:User','u')->join('u.listChoices','lc');
        $users = $qb->getQuery()->getResult();
        if(empty($users)){
            return new View('no user found',Response::HTTP_NOT_FOUND);
        }
        return new View($users,Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     * resource="/user",
     * description="Create a new user",
     *     parameters = {
     *         { "name" = "pseudo", "dataType" = "string", "required" = "true", "description" = "user pseudo" },
     *         { "name" = "email", "dataType" = "string", "required" = "true", "description" = "user email" },
     *         { "name" = "birth_date","dataType" = "Date",  "format" = "1991/03/26", "required" = "true", "description" = "user email" },
     *     },
     * statusCodes={
     *      200 = "user created",
     *      302 = "user already exist"
     *     }
     * )
     *
     * @param Request $request
     * @return View
     * @Rest\Post("/user")
     */
    public function createUserAction(Request $request){
        $serializer = new Serializer(array(new DateTimeNormalizer('d/m/Y')));

        $tmpUser = $this->getDoctrine()->getRepository('MovieBundle:User')->findOneBy(['email'=>$request->get('email')]);
        if($tmpUser != null){
            return new View("user already exist",Response::HTTP_FOUND);
        }

        $em = $this->getDoctrine()->getManager();
        $user = new User();
        $user->setPseudo($request->get('pseudo'));
        $user->setEmail($request->get('email'));
        $birthDate  = $serializer->denormalize($request->get('bdate'), \DateTime::class);
        $user->setBirthDate($birthDate);
        $user->setCreatedAt(new \DateTime());
        $em->persist($user);
        $em->flush();

        return new View("user created",Response::HTTP_OK);

    }



}
