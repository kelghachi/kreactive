<?php

namespace MovieBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use MovieBundle\Entity\UserMovie;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MovieController extends FOSRestController
{

    /**
     * @ApiDoc(
     * resource="/movieChoice",
     * description="Creates new User Movie Choice in current week",
     *  parameters = {
     *         { "name" = "user", "dataType" = "integer", "required" = "true", "description" = "user id" },
     *         { "name" = "movie", "dataType" = "integer", "required" = "true", "description" = "movie id" }
     *     },
     * statusCodes={
     *      200 = "Your movie is successfully choosed",
     *      302 = "Movie Already chosen",
     *      404 = "User or Movie not found",
     *      208 = "Already chosen three movies in currennt week"
     *     }
     * )
     *
     * @param Request $request
     * @return View
     * @Rest\Post("/movieChoice")
     */
    public function createUserMovieAction(Request $request)
    {
        $userId = $request->get('user');
        $movieId = $request->get('movie');

        $movieChoosed =  $this->getDoctrine()
            ->getRepository('MovieBundle:UserMovie')
            ->findOneBy(['user'=>$userId,'movie'=>$movieId]);

        if($movieChoosed != null){
            return new View("You have already chosen this movie",Response::HTTP_FOUND);
        }

        $user = $this->getDoctrine()->getRepository("MovieBundle:User")->find($userId);
        if($user == null){
            return new View("User not found",Response::HTTP_NOT_FOUND);
        }

        $movie = $this->getDoctrine()->getRepository("MovieBundle:Movie")->find($movieId);
        if($movie == null){
            return new View("Movie not found",Response::HTTP_NOT_FOUND);
        }

        $em = $this->getDoctrine()->getManager();

        $from = new \DateTime;
        $from->setIsoDate($from->format('o'), $from->format('W'));
        $to = clone $from;
        $to->modify('+6 day');

        $moviesChosenThisWeek = $em->createQuery('SELECT um.id, count(um.id) As score FROM MovieBundle:UserMovie um WHERE  um.user = :user_id AND um.chosedAt BETWEEN :firstDay AND :lastDay')
                                   ->setParameters(["user_id" => $user, "firstDay" => $from->format('Y-m-d'),"lastDay" => $to->format('Y-m-d')])
                                   ->getResult();
        if(!empty($moviesChosenThisWeek) && $moviesChosenThisWeek[0]['score'] == 3){
            return new View("You have already chosen 3 movies this week",Response::HTTP_ALREADY_REPORTED);
        }
        $userMovie = new UserMovie();
        $userMovie->setUser($user);
        $userMovie->setMovie($movie);
        $userMovie->setChosedAt(new \DateTime());

        $em->persist($userMovie);
        $em->flush();
        return new View("Your movie is successfully choosed",Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     * resource="/movieChoice/{id}",
     * description="Deletes an Movie Choice",
     *  parameters = {
     *         { "name" = "id", "dataType" = "integer", "required" = "true", "description" = "Movie Choice id" },
     *     },
     * statusCodes={
     *      200 = "Your Movie choice deleted successfully",
     *      404 = "Movie Choice not found"
     *     }
     * )
     *
     * @param $id
     * @Rest\Delete("/movieChoice/{id}")
     * @return View
     */
    public function deleteUserMovieAction($id){
        $movieChoice = $this->getDoctrine()->getRepository("MovieBundle:UserMovie")->find($id);
        $em = $this->getDoctrine()->getManager();
        if($movieChoice == null){
            return new View("This choice is not found in your movie choices",Response::HTTP_NOT_FOUND);
        }
        $em->remove($movieChoice);
        $em->flush();
        return new View("Your Movie choice deleted successfully",Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     * resource="/movieChoice/{userId}",
     * description="Returns user chosen movies",
     *  parameters = {
     *         { "name" = "userId", "dataType" = "integer", "required" = "true", "description" = "User id" },
     *     },
     * statusCodes={
     *      200 = "List Of movie choices",
     *      404 = "User not found"
     *     }
     * )
     *
     * @param $userId
     * @Rest\Get("/movieChoice/{userId}")
     * @return View
     */
    public function getUserChoicesAction($userId){
        $user = $this->getDoctrine()->getRepository("MovieBundle:User")->find($userId);
        if (empty($user)){
            return new View("User not found",Response::HTTP_NOT_FOUND);
        }
        return new View($user->getListChoices(),Response::HTTP_OK);
    }

    /**
     * @ApiDoc(
     * resource="/bestMovie/",
     * description="Returns the best movie chosen in the current week",
     * statusCodes={
     *      200 = "Best Movie chosen",
     *      404 = "No Movie Found"
     *     }
     * )
     * @Rest\Get("/bestMovie")
     */
    public function bestChosenMovieAction(){
        $em = $this->getDoctrine()->getManager();
        $from = new \DateTime;
        $from->setIsoDate($from->format('o'), $from->format('W'));
        $to = clone $from;
        $to->modify('+6 day');

        $bestMovieId = $em->createQuery('SELECT um.id, count(um.movie) As HIDDEN score FROM MovieBundle:UserMovie um WHERE um.chosedAt BETWEEN :firstDay AND :lastDay GROUP BY um.movie ORDER BY score desc')
            ->setParameters(["firstDay" => $from->format('Y-m-d'),"lastDay" => $to->format('Y-m-d')])
            ->getResult();

        if(empty($bestMovieId)){
            return new View("No Movie Found",Response::HTTP_NOT_FOUND);
        }
        $bestMovie = $this->getDoctrine()->getManager()->createQueryBuilder()->select('m')
            ->from('MovieBundle:Movie','m')
            ->where('m.id = :id')
            ->setParameter('id',$bestMovieId[0]['id'])
            ->getQuery()
            ->getResult();
        return $bestMovie;
    }
}
